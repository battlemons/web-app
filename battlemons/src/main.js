import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import firebase from "firebase";
import VueSocketIO from "vue-socket.io";

/* Import specific components */
import ChoosePokemonMenu from "./components/ChoosePokemonMenu";
import ActionMenu from "./components/ActionMenu";
import MovesMenu from "./components/MovesMenu";
import UserField from "./components/UserField";
import UserPokemonInfo from "./components/UserPokemonInfo";
import UserPokemonImage from "./components/UserPokemonImage";
import OpponentField from "./components/OpponentField";
import OpponentPokemonInfo from "./components/OpponentPokemonInfo";
import OpponentPokemonImage from "./components/OpponentPokemonImage";
import GameOver from "./components/GameOver.vue";

Vue.config.productionTip = false;
Vue.component("choosepokemon-menu", ChoosePokemonMenu);
Vue.component("action-menu", ActionMenu);
Vue.component("user-field", UserField);
Vue.component("user-pokemon-info", UserPokemonInfo);
Vue.component("user-pokemon-image", UserPokemonImage);
Vue.component("opponent-field", OpponentField);
Vue.component("opponent-pokemon-info", OpponentPokemonInfo);
Vue.component("opponent-pokemon-image", OpponentPokemonImage);
Vue.component("moves-menu", MovesMenu);
Vue.component("game-over", GameOver);

let app = "";

var firebaseConfig = {
  apiKey: "AIzaSyDKPKaRBT0WvS9x_e7FTSJCKIcyet7tpdQ",
  authDomain: "battlemons-4eadb.firebaseapp.com",
  databaseURL: "https://battlemons-4eadb.firebaseio.com",
  projectId: "battlemons-4eadb",
  storageBucket: "battlemons-4eadb.appspot.com",
  messagingSenderId: "655764190089",
  appId: "1:655764190089:web:cfc686f7c9d4a71eb6e758",
  measurementId: "G-P5RP0N4VDC"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

Vue.use(
  new VueSocketIO({
    debug: true,
    connection: "http://localhost:8383",
    vuex: {
      store,
      actionPrefix: "SOCKET_",
      mutationPrefix: "SOCKET_"
    }
  })
);

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount("#app");
  }
});
